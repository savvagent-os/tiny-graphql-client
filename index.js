'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var tinyFetchInterceptors = require('@savvagent-os/tiny-fetch-interceptors');

// import { SubscriptionClient } from 'subscriptions-transport-ws';

const getOpname = /(query|mutation) ?([\w\d-_]+)? ?\(.*?\)? \{/;

class GraphQLClient {
  constructor(options = {}) {
    this.url = options.url;
    this.options = options;
    this.options.headers = this.options.headers || {};
    this.fetchClient = options.fetchClient || new tinyFetchInterceptors.FetchClient([ tinyFetchInterceptors.jsonRequest, tinyFetchInterceptors.rejectErrors, tinyFetchInterceptors.extractData ]);
    // this.wsc = new SubscriptionClient(this.url.replace(/http/, 'ws'), { reconnect: true });
  }

  /**
   * [builder description]
   * @param  {[type]} str [description]
   * @return {[type]}     [description]
   *
   * This method came from nanoql. It is under an MIT license.
   * https://github.com/yoshuawuyts/nanographql
   Copyright (c) 2016 Yoshua Wuyts

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in all
   copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   SOFTWARE.
   */
  builder(str) {
    const _str = Array.isArray(str) ? str.join('') : str;
    const name = getOpname.exec(_str);
    return function(variables) {
      const data = { query: _str };
      if (variables) data.variables = JSON.stringify(variables);
      if (name && name.length) {
        const operationName = name[2];
        if (operationName) data.operationName = name[2];
      }
      return JSON.stringify(data);
    };
  }

  request(params) {
    const { query } = params;
    if (typeof query !== 'function') throw new SyntaxError('query must be builder function');
    const variables = params.variables || {};
    const headers = params.headers || {};
    const body = query(variables);
    Object.assign(this.options.headers, headers);
    const fetchConfig = Object.assign({ body, method: 'POST', headers });
    return this.fetchClient.request(this.url, fetchConfig).then(resp => resp.data);
  }
}

exports.GraphQLClient = GraphQLClient;
