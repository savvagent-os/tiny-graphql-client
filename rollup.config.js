const path = require('path');
// eslint-disable-next-line
const root = process.cwd();

const input = path.resolve(root, 'src', 'GraphQLClient.js');

export default {
  input,
  output: [
    {
      file: path.resolve(root, 'dist', 'tiny-graphql-client.js'),
      format: 'es'
    },
    {
      file: path.resolve(root, 'index.js'),
      format: 'cjs'
    },
    {
      file: path.resolve(root, 'index.mjs'),
      format: 'es'
    }
  ]
};
